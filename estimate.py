# Copyright (C) 2016 Jack S. Hale, Paul Hauseux.
#
# This file is part of stochastic-hyperelasticity.
#
# stochastic-hyperelasticity is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# stochastic-hyperelasticity is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with stochastic-hyperelasticity. If not, see <http://www.gnu.org/licenses/>.

"""Monte Carlo and Sensitivity Monte Carlo estimators implemented using
numpy.

Note that there is no FEniCS/DOLFIN code in this file. If your model
can output the required sensitivity derivative data, then you can work
with a modification of this code.
"""
from __future__ import print_function

import numpy as np

import chaospy as cp

from forward_tlm_models import *

# Position of quantity of interest in domain
x_p = (lx/2.0, 0.0, 0.0) 

# Quantity of interest functionals as defined in paper
# First moment
psi_1 = lambda u: np.array([u(x_p)[2]])

def estimate(num_samples, psi_1=psi_1):
    """Estimate using standard and sensivity derivative enhanced Monte Carlo.

    Args:
        num_samples (int): Number of samples.
    
    Returns:
        np.ndarray (float): Array of shape (4,) with 
    """
    # Draw samples from our known stochastic distribution
    # for the Young's modulus. 
    E_det = 10000.0
    dist = cp.beta(2, 2)
    omegas = dist.sample(num_samples)
    omegas = (E_det/2.0)*(1.0 + 2.0*omegas)
    omega_bar = (E_det/2.0)*(1.0 + 2.0*cp.E(dist))

    # We store all of the functional samples. A better real-world implementation
    # for this would be to only store the current sample and the previous estimate
    # and do the Monte Carlo estimation as an update, e.g. an accumulator.
    
    # Dummy run, get shape of the output of the estimators.
    u = forward_model(omega_bar)
    psi_1_length = psi_1(u).shape[0]
    
    # Space to store each sample.
    psi_1_zs = np.zeros([num_samples, psi_1_length])
    
    # Run forward mode, store output of quantity of interest.
    for i, omega in enumerate(omegas):
        print("Realisation {}...".format(i))
        u = forward_model(omega)
        psi_1_zs[i, :] = psi_1(u)
        print("")

    ## Standard Monte Carlo for comparison 
    def mc_estimator(psi_zs):
        E_psi = np.mean(psi_zs, axis=0)
        return E_psi

    ## Sensitivity derivative Monte Carlo procedure.
    def sd_mc_estimator(psi_zs, dpsi_dm, omegas):
        correction = dpsi_dm*(omegas - omega_bar)[:, np.newaxis]
        E_psi = np.mean(psi_zs - correction, axis=0)
        return E_psi

    # Calculate derivative of the first moment with respect to parameter at the
    # mean value of the parameter. This would be more elegant with the adjoint
    # approach to automatic differentiation.
    print("Tangent linear model...")
    dpsi_1_dm = psi_1(tangent_linear_model(omega_bar))
    
    # Calculate evolution of estimators with increasing samples.
    # Bad way of doing this.
    E_psi_mc = np.zeros(len(omegas))
    E_psi_sd_mc = np.zeros(len(omegas))
    for i in range(0, len(omegas)):
        cut_samples = psi_1_zs[0:i+1]
        cut_omegas = omegas[0:i+1]

        E_psi_mc[i] = mc_estimator(cut_samples)
        E_psi_sd_mc[i] = sd_mc_estimator(cut_samples, dpsi_1_dm, cut_omegas)

    return (E_psi_mc, E_psi_sd_mc)


def main():
    """Quick run with 50 samples."""
    
    from dolfin import set_log_level, WARNING
    set_log_level(WARNING)
    samples = 50 
    print("Estimating using {} samples...".format(samples)) 
    E_psi_mc, E_psi_sd_mc = estimate(samples)

    print("MC estimator expectation: {}".format(E_psi_mc[-1]))
    print("SDMC estimator expectation: {}".format(E_psi_sd_mc[-1]))
   
    np.savetxt("output/MC.txt", E_psi_mc)
    np.savetxt("output/SD_MC.txt", E_psi_sd_mc)


if __name__ == "__main__":
    main()
