# Copyright (C) 2016 Jack S. Hale, Paul Hauseux.
#
# This file is part of stochastic-hyperelasticity.
#
# stochastic-hyperelasticity is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# stochastic-hyperelasticity is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with stochastic-hyperelasticity. If not, see <http://www.gnu.org/licenses/>.

import numpy as np
import matplotlib as mpl
import seaborn as sns
import matplotlib.pyplot as plt

sns.set_style("whitegrid")

E_MC = np.loadtxt("output/MC.txt")
E_SD_MC = np.loadtxt("output/SD_MC.txt")

sns.set_palette("colorblind")
fig = plt.figure(figsize=((3.5, 2.8)))
ax = plt.subplot(111)

x = range(1, len(E_MC) + 1)

ax.plot(x, E_MC, label="MC", linewidth=1.0)
ax.plot(x, E_SD_MC, label="SD-MC", linewidth=1.0)
ax.set_ylabel("$\mathbb{E}[U_z]$ (m)")
ax.set_xlabel("$Z$")
plt.xlim(0, x[-1])
plt.legend(loc='lower right', frameon=True)

plt.tight_layout()
plt.savefig("output/evolution.pdf")
plt.savefig("output/evolution.png")
