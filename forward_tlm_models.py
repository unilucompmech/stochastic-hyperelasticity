# Copyright (C) 2016 Jack S. Hale, Paul Hauseux.
#
# This file is part of stochastic-hyperelasticity.
#
# stochastic-hyperelasticity is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# stochastic-hyperelasticity is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with stochastic-hyperelasticity. If not, see <http://www.gnu.org/licenses/>.

"""Implementation of the forward and tangent linear models of the Neo-Hookean
hyperelasticity equations in DOLFIN.

This file gives a template  with which you can express many different types of
steady-state PDEs using FEniCS and use in conjunction with the sensitivity
derivative Monte Carlo method.
"""

import numpy as np
from dolfin import *

# mesh
n = 10
lx = 0.1
ly = 0.01
lz = 0.01
mesh = BoxMesh(Point(0,0,0), Point(lx, ly, lz), 20, 10, 10)
V = VectorFunctionSpace(mesh, "Lagrange", 1)

def forward_full_model(omega, V=V):
    """Forward model of Neo-Hookean hyperelasticity equation.

    See also: forward(E), a light wrapper around this function.

    Args:
        omega (float): Young's modulus.
        V (dolfin.FunctionSpace): space on which to solve problem.
    
    Returns:
        The solution (dolfin.Function).
        Test function (dolfin.TestFunction).
        Trial function (dolfin.TrialFunction).
        Residual (ufl.Form).
        Parameter (ufl.variable).
    """
        # Define functions
    du = TrialFunction(V)            # Incremental displacement
    v  = TestFunction(V)             # Test function
    u  = Function(V)                 # Displacement from previous iteration

    # Kinematics
    I = Identity(3)             # Identity tensor dimension 3
    F = I + grad(u)             # Deformation gradient
    C = F.T*F                   # Right Cauchy-Green tensor

    # Invariants of deformation tensors
    J  = det(F)
    I1 = tr(C)
    I2 = 0.5*(tr(C)**2-tr(C*C))
    I1bar = J**(-2.0/3.0)*I1
    I2bar = J**(-4.0/3.0)*I2

    # Boundary conditions
    left  =   CompiledSubDomain("near(x[0], side) && on_boundary", side = 0.0)
    right =   CompiledSubDomain("near(x[0], side) && on_boundary", side = lx)
    top   =   CompiledSubDomain("near(x[2], side) && on_boundary", side = lz)

    boundaries = FacetFunction("size_t", mesh)
    boundaries.set_all(0)
    left.mark(boundaries, 1)
    right.mark(boundaries, 2)
    top.mark(boundaries, 3)

    bcs1 = DirichletBC(V, Constant([0.0, 0.0, 0.0]), boundaries, 1)
    bcs2 = DirichletBC(V, Constant([0.0, 0.0, 0.0]), boundaries, 2)
    bcs = [bcs1, bcs2]

    # Surface force
    f = Constant((0.0, 0.0, -50.))
    ds = Measure('ds', domain=mesh, subdomain_data=boundaries)
    continuation_steps = 5 
    t = 1.0/continuation_steps
    dt = float(t)
    T = 1.0 + 1e-5
    time = Expression("t", t=t, degree=0)

    # Elasticity parameters
    E_var, nu = variable(Constant(omega)), Constant(0.3)
    mu, lmbda = E_var/(2*(1 + nu)), E_var*nu/((1 + nu)*(1 - 2*nu))

    # Invariants of deformation tensors
    Ic = tr(C)
    J  = det(F)

    # Stored strain energy density (compressible neo-Hookean model)
    psi = (mu/2)*(Ic - 3) - mu*ln(J) + (lmbda/2)*(ln(J))**2

    # Total potential energy
    Pi = psi*dx - time*dot(f, u)*ds(3)

    # Compute first variation of Pi (directional derivative about u in the direction of v)
    F = derivative(Pi, u, v)

    # Compute Jacobian of F
    J = derivative(F, u, du)

    solver_parameters = {"nonlinear_solver": "snes",
                         "snes_solver":
                         {"linear_solver": "lu",
                          "line_search": "basic",
                          "maximum_iterations": 30,
                          "relative_tolerance": 1e-6,
                          "error_on_nonconvergence": False}}

    while t <= T:
        print("Load increment = {0}".format(t))
        time.t = t
        solve(F == 0, u, bcs, J=J, bcs=bcs, solver_parameters=solver_parameters)
        t += dt

    return (u, du, v, F, bcs, E_var)


def forward_model(nu, **kwargs):
    """Returns just the solution from the function forward_full."""
    return forward_full_model(nu, **kwargs)[0]


def tangent_linear_model(nu, V=V):
    """Tangent linear model of Neo-Hookean hyperelasticity equation.

    Args:
        E (float): Young's modulus.
        V (dolfin.FunctionSpace): space on which to solve problem.

    Returns:
        The tangent linear solution (dolfin.Function).
    """
    # First we solve the forward model and get back all the necessary parts to
    # calculate the tangent linear model.
    u, du, v, F, bcs, E_var = forward_full_model(nu, V)
    
    # From definition of tangent linear model.
    a = derivative(F, u, du)
    L = -diff(F, E_var)
    
    dudE = Function(V)
    A, b = assemble_system(a, L, bcs=bcs) 
   
    # Tangent linear model is always linear.
    solve(A, dudE.vector(), b, "lu")

    return dudE    


def main():
    u, du, v, F, bcs, E_var = forward_full_model(10000.0)
    
    centre = (lx/2.0, 0.0, 0.0)
    z_displacement = u(centre)[2]
    print("Displacement at centre of beam: {0:.6f}".format(z_displacement))
    XDMFFile("output/forward_displacement_field.xdmf").write(u)

    dudE = tangent_linear_model(10000.0)
    sensitivity = dudE(centre)[2]
    print("Sensitivity at centre of beam: {0}".format(sensitivity)) 

if __name__ == "__main__":
    main()
