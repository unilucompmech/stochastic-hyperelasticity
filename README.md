# stochastic-hyperelasticity
## Solving the Stochastic Hyperelasticity Equation

### Description

The repository contains a code to solve the stochastic neo-Hookean hyperelastic
equation using a sensitivity derivative enhanced Monte Carlo method. We show
the improved evolution of this estimator with respect to the standard Monte
Carlo method.

This example is shown in the paper:

Quantifying the uncertainty in a hyperelastic soft tissue model with stochastic parameters, Hauseux, Hale, Cotin, Bordas. Submitted. https://hdl.handle.net/10993/30946

You may also be interested in the code at
https://doi.org/10.6084/m9.figshare.3561306 which shows the expected
O(1/Z^{1/2}) convergence rate of the method for a stochastic Burgers equation.

![evolution](https://bytebucket.org/unilucompmech/stochastic-hyperelasticity/raw/08310e508385c0dbfc4c9d5433e9cc8cf18ad726/output-paper/evolution.png?token=f631c2d04824e464bff1f5fe5df986bbae564478)

### Key features

* *Concise:* Around 50 lines of Python to express and solve non-linear forward
  and tangent linear problems. Monte Carlo estimators around 80 lines of
  Python. Monte Carlo estimator code is independent of forward and tangent linear
  problem specification in FEniCS.

* *Adaptable:* It should be possible to adapt this code to many different PDEs if you can
  express your problem in the Unified Form Language of the FEniCS Project. More
  advanced construction of tangent linear and adjoint models is available using
  [dolfin-adjoint](http://dolfin-adjoint.org).

* *Easy to run*: A Docker image is provided at `jhale/stochastic-hyperelasticity` to
  run the code in this repository. The result in the paper was produced using
  the image hash:

### Note

Monte Carlo estimators are inherently random processes. You should not expect
*exactly* the same answer as the paper when you run this code. However the
overall trends should be evident.

### Instructions

1. Install Docker for your platform following the instructions at
   [Docker.com](https://docker.com).

2. Clone this repository:

        git clone https://bitbucket.org/unilucompmech/stochastic-hyperelasticity

3. Enter the directory of the repository, and launch the Docker container using
   the command:

        cd stochastic-hyperelasticity
        ./launch-container.py

4. You should be presented with a terminal inside the container. The main file
   that runs the Monte Carlo estimation procedure  is `estimate.py`. You can
   run it using:

        python estimate.py

5. By default, `estimate.py` generates 50 samples. The results of the
   standard and sensitivity derivative Monte Carlo estimator are printed to the
   screen and the evolution of the estimators are written to two text files in
   `output/`.

7. And then generate a plot of the evolution of the two estimators with:

        python plot.py

### Description of files

* `estimate.py`: Implementation of the standard and sensitivity derivative
  Monte Carlo estimator. Calls forward and tangent linear models in
  `forward_tlm_models.py`.

* `forward_tlm_models.py` DOLFIN/UFL implementation of forward and tangent
  linear models of the neo-Hookean hyperelasticity model. Running the script with
  `python forward_tlm_models.py` will output a single solution in `output/` at
  the mean parameter.

* `plot.py`: This file plots the evolution of the two estimators. You should
  run `estimate.py` first. 

* `paper-output/*`: Raw results and generated plots from the paper.
 
### Issues and Support

For support or questions please email [jack.hale@uni.lu](mailto:jack.hale@uni.lu).

### Authors

Paul Hauseux, University of Luxembourg, Luxembourg.

Jack S. Hale, University of Luxembourg, Luxembourg.

### License 

stochastic-hyperelasticity is free software: you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more
details.

You should have received a copy of the GNU Lesser General Public License along
with stochastic-hyperelasticity.  If not, see <http://www.gnu.org/licenses/>.